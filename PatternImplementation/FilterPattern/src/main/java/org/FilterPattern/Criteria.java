package org.FilterPattern;

import java.util.List;

public interface Criteria {
	public List<Singer> meetCriteria(List<Singer> singers);
}
