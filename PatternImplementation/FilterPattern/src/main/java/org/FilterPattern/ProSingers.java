package org.FilterPattern;

import java.util.ArrayList;
import java.util.List;

public class ProSingers implements Criteria {

	public List<Singer> meetCriteria(List<Singer> singers) {
		List<Singer> proSingers=new ArrayList<Singer>();
		
		for(Singer singer : singers){
			if(singer.getPro()==true){
				proSingers.add(singer);
			}
		}
		return proSingers;
	}

}
