package org.FilterPattern;

import java.util.List;

public class AndCriteria implements Criteria {
	Criteria firstCriteria;
	Criteria secondCriteria;
	
	public AndCriteria(Criteria first,Criteria second) {
		this.firstCriteria=first;
		this.secondCriteria=second;
	}

	public List<Singer> meetCriteria(List<Singer> singers) {
		List<Singer> firstList=firstCriteria.meetCriteria(singers);		
		return secondCriteria.meetCriteria(firstList);
	}

}
