package org.FilterPattern;

import java.util.List;

public class OrCriteria implements Criteria {
	Criteria firstCriteria;
	Criteria secondCriteria;
	
	public OrCriteria(Criteria first, Criteria second) {
		this.firstCriteria=first;
		this.secondCriteria=second;
	}

	public List<Singer> meetCriteria(List<Singer> singers) {
		List<Singer> firstList=firstCriteria.meetCriteria(singers);
		List<Singer> secondList=secondCriteria.meetCriteria(singers);
		for(Singer singer : secondList){
			if(!firstList.contains(singer)){
				firstList.add(singer);
			}
		}
		return firstList;
	}

}
