package org.FilterPattern;

public class Singer {
	private String name;
	private String gender;
	private Boolean pro;
	
	public Singer(String name,String gender, Boolean pro){
		this.name=name;
		this.gender=gender;
		this.pro=pro;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public void setGender(String gender){
		this.gender=gender;
	}
	
	public void setPro(Boolean pro){
		this.pro=pro;
	}
	
	public String getName(){
		return name;
	}
	
	public String getGender(){
		return gender;
	}
	
	public Boolean getPro(){
		return pro;
	}
}
