package org.FilterPattern;

import java.util.ArrayList;
import java.util.List;

public class Client 
{
    public static void main( String[] args )
    {
     List<Singer> singers=new ArrayList<Singer>();
     
     singers.add(new Singer("Tiziano Ferro","Maschio",true));
     singers.add(new Singer("Giusi Ferreri","Femmina",true));
     singers.add(new Singer("Vasco Rossi","Maschio",true));
     singers.add(new Singer("Noemi","Femmina",true));
     singers.add(new Singer("Giorgio","Maschio",false));
     singers.add(new Singer("Miriam","Femmina",false));
     
     Criteria male=new MaleSingers();
     Criteria female=new FemaleSingers();
     Criteria pro=new ProSingers();
     Criteria maleAndPro=new AndCriteria(male, pro);
     Criteria femaleOrPro=new OrCriteria(female, pro);
     
     System.out.println("Maschi:\n");
     printSingers(male.meetCriteria(singers));
     System.out.println("\nFemmine:\n");
     printSingers(female.meetCriteria(singers));
     System.out.println("\nProfessionisti Maschi:\n");
     printSingers(maleAndPro.meetCriteria(singers));
     System.out.println("\nFemmine o Professionisti:\n");
     printSingers(femaleOrPro.meetCriteria(singers));
     
    }
    
    public static void printSingers(List<Singer> singers){
    	for(Singer singer : singers){
    		System.out.print("Cantante: [Name: "+singer.getName()+", Gender: "+singer.getGender());
    		if(singer.getPro()==true){
    			System.out.print(", Descrizione: Professionista]\n");
    		}else System.out.print(", Descrizione: Dilettante]\n");
    	}
    }
}
