package org.FilterPattern;

import java.util.ArrayList;
import java.util.List;

public class MaleSingers implements Criteria {

	public List<Singer> meetCriteria(List<Singer> singers) {
		List<Singer> maleSingers=new ArrayList<Singer>();
		
		for(Singer singer : singers){
			if(singer.getGender().equalsIgnoreCase("MASCHIO")){
				maleSingers.add(singer);
			}
		}
		return maleSingers;
	}

}
