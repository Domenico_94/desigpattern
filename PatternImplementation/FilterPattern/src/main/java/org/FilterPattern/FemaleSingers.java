package org.FilterPattern;

import java.util.ArrayList;
import java.util.List;

public class FemaleSingers implements Criteria {

	public List<Singer> meetCriteria(List<Singer> singers) {
		List<Singer> femaleSingers=new ArrayList<Singer>();
		
		for(Singer singer : singers){
			if(singer.getGender().equalsIgnoreCase("FEMMINA")){
				femaleSingers.add(singer);
			}
		}
		return femaleSingers;
	}

}
