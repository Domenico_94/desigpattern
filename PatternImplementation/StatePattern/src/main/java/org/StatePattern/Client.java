package org.StatePattern;

public class Client 
{
    public static void main( String[] args ){
    	Context context=new Context();
    	
    	StateOne stateOne=new StateOne();
    	stateOne.doThing(context);
    	
    	System.out.println(context.getState().toString());
    	
    	StateTwo stateTwo=new StateTwo();
    	stateTwo.doThing(context);
    	
    	System.out.println(context.getState().toString());
    	
    	StateThree stateThree=new StateThree();
    	stateThree.doThing(context);
    	
    	System.out.println(context.getState().toString());
    	
     
    }
}
