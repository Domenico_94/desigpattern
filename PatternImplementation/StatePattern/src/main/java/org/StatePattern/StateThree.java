package org.StatePattern;

public class StateThree implements State {
	private String memory;

	public void doThing(Context context) {
		memory="First of all, do you give me a name?";
		System.out.println("You are in the State Three, in memory: "+memory);
		context.setState(this);
	}
	
	public String toString(){
		return "State Three";
	}
	

}
