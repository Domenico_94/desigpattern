package org.StatePattern;

public class StateOne implements State {
	private String memory;

	public void doThing(Context context) {
		memory="Hello User";
		System.out.println("You are in the "+this+", in memory: "+memory);
		context.setState(this);
	}
	
	public String toString(){
		return "State One";
	}

}