package org.StatePattern;

public class StateTwo implements State {
	private String memory;

	public void doThing(Context context) {
		memory="I'm your CPU Friend, ask me anything you want.";
		System.out.println("You are in the State Two, in memory: "+memory);
		context.setState(this);
	}
	
	public String toString(){
		return "State Two";
	}

}
