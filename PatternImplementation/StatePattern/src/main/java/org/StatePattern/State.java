package org.StatePattern;

public interface State {
	public void doThing(Context context);
}
