package org.AbstractFactoryPattern;

public class Client 
{
    public static void main( String[] args )
    {
    	//mi dirigo al bancone del bar e consumo
       Assistant assistant= Seller.getAssistant("Bar");
       Bar coffe=assistant.getDrink("caffe");
       coffe.drink();
       Bar water = assistant.getDrink("water");
       water.drink();
       
       //mi dirigo al bancone dei gelati e consumo
       assistant = Seller.getAssistant("icecream");
       IceCreamShop iceCreamCone = assistant.getIceCream("cono");
       iceCreamCone.eat();       
       IceCreamShop iceCreamCup = assistant.getIceCream("coppa");
       iceCreamCup.eat();
    }
}
