package org.AbstractFactoryPattern;

public class AssistantBar extends Assistant{
	public Bar getDrink(String drinkType){
		if(drinkType.equalsIgnoreCase("CAFFE")){
			return AssistantCoffe.getDrinkCaffe();
		}else if(drinkType.equalsIgnoreCase("WATER")){
			return AssistantWater.getDrinkWater();
		}
		
		return null;
	}

	@Override
	IceCreamShop getIceCream(String iceCreamType) {
		return null;
	}

}
