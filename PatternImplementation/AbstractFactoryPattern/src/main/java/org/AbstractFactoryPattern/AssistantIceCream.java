package org.AbstractFactoryPattern;

public class AssistantIceCream extends Assistant{
	public IceCreamShop getIceCream(String iceCreamType){
		if(iceCreamType.equalsIgnoreCase("CONO")){
			return AssistantCone.getIceCreamCone();
		}else if(iceCreamType.equalsIgnoreCase("COPPA")){
			return AssistantCup.getIceCreamCup();
		}
		
		return null;
	}

	@Override
	Bar getDrink(String drikType) {
		return null;
	}
}
