package org.AbstractFactoryPattern;

public class Seller {
 public static Assistant getAssistant(String assistantType){
	 if(assistantType.equalsIgnoreCase("BAR")){
		 return new AssistantBar();
	 }else if(assistantType.equalsIgnoreCase("ICECREAM")){
		 return new AssistantIceCream();
	 }
	 
	 return null;
 }
}
