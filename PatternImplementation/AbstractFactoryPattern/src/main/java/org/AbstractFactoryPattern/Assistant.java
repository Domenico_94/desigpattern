package org.AbstractFactoryPattern;

public abstract class Assistant {
	abstract IceCreamShop getIceCream(String iceCreamType);
	abstract Bar getDrink(String drikType);
}
