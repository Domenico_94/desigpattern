package org.AbstractFactoryPattern;

public class AssistantWater extends AssistantBar {

	public static Bar getDrinkWater() {
		return new Water();
	}

}
