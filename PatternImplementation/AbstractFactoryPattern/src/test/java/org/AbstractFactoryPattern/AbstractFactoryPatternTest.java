package org.AbstractFactoryPattern;


import static org.junit.Assert.*;
import org.junit.*;

/**
 * Unit test 
 */
public class AbstractFactoryPatternTest {    

    private static String assistant=null;
    private static String drink=null;
	
   
    
   @Test
    public void sellerBarCaffeShouldReturnAssistantBarWithCaffe()
    {
    	
    	Bar caffe = Seller.getAssistant(assistant).getDrink(drink);    	
        assertEquals("Jhonny sto bevendo il caffè!", caffe.drink());     
        assertEquals(new AssistantBar().getClass().getName(), Seller.getAssistant(assistant).getClass().getName());
    }
    
   @Test
    public void sellerBarShouldReturnAssistantBar()
    {
    	   	
    	Assistant ass = Seller.getAssistant(assistant);    	
        assertNotSame(new AssistantIceCream().getClass().getName(), ass.getClass().getName());
    }
    
   @Test
	public void newAssistantShouldNotBeNull()
    {
    	String assistantsType[] = {"bar","ICECREAM"};
    	for(int i=0; i<2; i++){
    		Assistant ass = Seller.getAssistant(assistantsType[i]);  
    		assertNotNull(ass);
    	}           
    }   
   
   @AfterClass
   public static void resetEnv(){
	   assistant=null;
	   drink=null;
   }
   
   @BeforeClass
   public static void initEnvironment() {
	   assistant = "bar";
	   drink = "caffe";
   }
}
