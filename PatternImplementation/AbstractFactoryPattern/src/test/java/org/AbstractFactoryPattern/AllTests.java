package org.AbstractFactoryPattern;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AbstractFactoryPatternTest.class })
public class AllTests {

}
