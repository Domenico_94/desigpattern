package org.PrototypePattern;

public class CarCompany extends Company {
	public CarCompany(){
		type="CarCompany";
	}
	
	@Override
	void call() {
		System.out.println("Sto effettuando la chiamata al CarCompany::call() method!");
	}

}
