package org.PrototypePattern;

public class SoftwareCompany extends Company {
	
	public SoftwareCompany() {
		type="SoftwareCompany";
	}

	@Override
	void call() {
		System.out.println("Sto effettuando la chiamata al SoftwareCompany::call() method!");
	}

}
