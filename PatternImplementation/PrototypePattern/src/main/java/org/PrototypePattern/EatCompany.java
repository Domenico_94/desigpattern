package org.PrototypePattern;

public class EatCompany extends Company {
	
	public EatCompany(){
		type="EatCompany";
	}

	@Override
	void call() {
		System.out.println("Sto effettuando la chiamata al EatCompany::call() method!");

	}

}
