package org.PrototypePattern;

import java.util.Hashtable;

public class CompanyCache {
	private static Hashtable<String,Company> companyMap=new Hashtable<String, Company>();
	
	public static Company getCompany(String id){
		Company company=companyMap.get(id);
		return (Company)company.clone();
	}
	
	public static void loadCache(){
		CarCompany carCompany=new CarCompany();
		carCompany.setId("1");
		companyMap.put(carCompany.getId(), carCompany);
		
		EatCompany eatCompany=new EatCompany();
		eatCompany.setId("2");
		companyMap.put(eatCompany.getId(), eatCompany);
		
		SoftwareCompany softwareCompany=new SoftwareCompany();
		softwareCompany.setId("3");
		companyMap.put(softwareCompany.getId(), softwareCompany);
	}
}
