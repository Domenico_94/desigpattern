package org.PrototypePattern;

public class Client 
{
    public static void main( String[] args )
    {
        CompanyCache.loadCache();
        
        Company clonedCompany1=(Company) CompanyCache.getCompany("1");
        System.out.println("Company: "+ clonedCompany1.getType());
        
        Company clonedCompany2=(Company) CompanyCache.getCompany("2");
        System.out.println("Company: "+ clonedCompany2.getType());
        
        Company clonedCompany3=(Company) CompanyCache.getCompany("3");
        System.out.println("Company: "+ clonedCompany3.getType());
    }
}
