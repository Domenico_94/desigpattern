package org.PrototypePattern;

public abstract class Company implements Cloneable {
	private String id;
	protected String type;
	
	public void setType(String type){
		this.type=type;
	}
	
	public void setId(String id){
		this.id=id;
	}
	
	public String getType(){
		return this.type;
	}
	
	public String getId(){
		return this.id;
	}
	
	abstract void call();
	
	public Object clone(){
		Object clone=null;
		
		try {
			clone=super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return clone;
	}
	
}
