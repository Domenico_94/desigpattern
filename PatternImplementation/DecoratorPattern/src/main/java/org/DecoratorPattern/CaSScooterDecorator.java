package org.DecoratorPattern;

public class CaSScooterDecorator extends ScooterDecorator {
	private String color;
	private Boolean style;   //"true" sport, "false" city.
	
	public CaSScooterDecorator(Scooter scooter,String color, Boolean style) {
		super(scooter);
		super.increment();
		this.color=color;
		this.style=style;
	}
	
	public void setColor(String color){
		this.color=color;
	}
	
	public void setStyle(Boolean style){
		this.style=style;
	}
	
	public String getColor(){
		return this.color;
	}

	public Boolean getStyle() {
		return style;
	}

	public void view() {
		super.view();
		System.out.print("\t[ Color: "+color+", Style: ");
		
		if(style==true){
			System.out.print("Sport ].\n");
		}else System.out.print("City ].\n");
	}

}
