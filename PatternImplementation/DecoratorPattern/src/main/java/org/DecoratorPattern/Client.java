package org.DecoratorPattern;

public class Client 
{
    public static void main( String[] args )
    {
     Scooter scooter=new Piaggio("Liberty","125");
     Scooter scooterOne=new Honda("SH","150");
     
     CaSScooterDecorator decoratorScooter=new CaSScooterDecorator(scooter,"Red",true); 
     
     DecScooterDecorator decoratorScooterOne=new DecScooterDecorator(scooterOne,"Viva la Vita!");
     
     
     System.out.println("Scooters without decorator\n");
     scooter.view();
     
     scooterOne.view();
     
     System.out.println("Scooters with decorator\n");
     decoratorScooter.view();
     
     decoratorScooterOne.view();
     
     
     
    }
}
