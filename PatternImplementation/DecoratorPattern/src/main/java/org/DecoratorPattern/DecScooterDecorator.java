package org.DecoratorPattern;

public class DecScooterDecorator extends ScooterDecorator {
	private String decals;

	public DecScooterDecorator(Scooter scooter, String decals) {
		super(scooter);
		super.increment();
		this.decals=decals;
	}
	
	public String getDecals() {
		return decals;
	}

	public void setDecals(String decals) {
		this.decals = decals;
	}

	@Override
	public void view() {
		super.view();
		System.out.println("\t[Decals: "+decals+" ].");
	}
}
