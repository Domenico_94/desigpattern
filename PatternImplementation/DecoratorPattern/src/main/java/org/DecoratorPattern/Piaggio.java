package org.DecoratorPattern;

public class Piaggio implements Scooter {
	private String type;
	private String cc;
	
	public Piaggio(String type,String cc){
		this.type=type;
		this.cc=cc;
	}

	public void view() {
		System.out.println("Scooter Piaggio : [ "+type+", "+cc+"cc ]");
	}

}
