package org.DecoratorPattern;

public class Honda implements Scooter{
	private String type;
	private String cc;
	
	public Honda(String type,String cc) {
		this.type=type;
		this.cc=cc;
	}

	public void view() {
		System.out.println("Scooter Honda : [ "+type+", "+cc+"cc ]");
		
	}

}
