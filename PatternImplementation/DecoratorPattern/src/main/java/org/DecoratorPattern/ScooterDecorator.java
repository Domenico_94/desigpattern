package org.DecoratorPattern;

public abstract class ScooterDecorator implements Scooter {
	private static int id;
	protected Scooter decoratedScooter;
	
	public ScooterDecorator(Scooter scooter) {
		this.decoratedScooter=scooter;
	}
	
	protected static void increment(){
		id=id+1;
	}

	public void view() {
		decoratedScooter.view();
	}

}
