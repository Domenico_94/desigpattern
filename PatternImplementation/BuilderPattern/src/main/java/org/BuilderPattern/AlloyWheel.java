package org.BuilderPattern;

public class AlloyWheel extends Tire {

	@Override
	public String getType() {
		return "Cerchio in Lega";
	}

	@Override
	public float getPrice() {
		return 1000f;
	}

}
