package org.BuilderPattern;

import java.util.ArrayList;
import java.util.List;

public class Car {
 private List<ItemCar> items=new ArrayList<ItemCar>();
 
 public void addItemCar(ItemCar itemCar){
	 items.add(itemCar);
 }
 
 public float getTotalPrice(){
	 float total=0.0f;
	 for(ItemCar itemCar :items){
		 total=total + itemCar.getPrice();
	 }
	 return total;
 }
 
 public void showItemCar(){
	 for(ItemCar itemCar :items){
		System.out.println("Tipo: "+itemCar.getType()+"\n");
		System.out.println("Prezzo: "+itemCar.getPrice()+"\n");
	 }
 }
}
