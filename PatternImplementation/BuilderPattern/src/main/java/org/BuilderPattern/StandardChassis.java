package org.BuilderPattern;

public class StandardChassis extends Chassis {

	@Override
	public String getType() {
		return "Telaio Standard";
	}

	@Override
	public float getPrice() {
		return 500f;
	}

}
