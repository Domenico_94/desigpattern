package org.BuilderPattern;

/**
 * Hello world!
 *
 */
public class Client 
{
    public static void main( String[] args )
    {	Car myCar;
    	Float money;
    	CarBuilder carBuilder=new CarBuilder();
    	myCar=carBuilder.composeFerrari();
    	System.out.println("Ferrari");
    	myCar.showItemCar();
    	money=myCar.getTotalPrice();
    	System.out.println("Prezzo Totale: "+money+"\n");
    	
    	
    	myCar=carBuilder.composeCinquecento();
    	System.out.println("Cinquecento");
    	myCar.showItemCar();
    	money=myCar.getTotalPrice();
    	System.out.println("Prezzo Totale: "+money+"\n");
    
    	
    }
}
