package org.BuilderPattern;

public class StandardWheel extends Tire {

	@Override
	public String getType() {
		return "Cerchione Standard";
	}

	@Override
	public float getPrice() {
		return 200f;
	}

}
