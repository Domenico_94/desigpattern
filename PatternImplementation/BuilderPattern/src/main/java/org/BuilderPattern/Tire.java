package org.BuilderPattern;

public abstract class Tire implements ItemCar{
	public abstract String getType();
	public abstract float getPrice();
}
