package org.BuilderPattern;

public class StandardEngine extends Engine {

	@Override
	public String getType() {
		return "Motore Standard";
	}

	@Override
	public float getPrice() {
		return 1000f;
	}

}
