package org.BuilderPattern;

public abstract class Chassis implements ItemCar {

	public abstract String getType();

	public abstract float getPrice();

}
