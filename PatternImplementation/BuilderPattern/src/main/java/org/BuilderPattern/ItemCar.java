package org.BuilderPattern;

public interface ItemCar {
	public String getType();
	public float getPrice();
}
