package org.BuilderPattern;

public class Carbon extends Chassis {

	@Override
	public String getType() {
		return "Telaio in Carbonio";
	}

	@Override
	public float getPrice() {
		return 5000f;
	}

}
