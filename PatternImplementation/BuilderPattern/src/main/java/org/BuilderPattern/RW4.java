package org.BuilderPattern;

public class RW4 extends Engine {

	@Override
	public String getType() {
		return "Motore RW4";
	}

	@Override
	public float getPrice() {
		return 20000f;
	}

}
