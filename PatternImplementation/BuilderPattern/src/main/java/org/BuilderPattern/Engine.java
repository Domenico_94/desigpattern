package org.BuilderPattern;

public abstract class Engine implements ItemCar {

	public abstract String getType();

	public abstract float getPrice();
}
