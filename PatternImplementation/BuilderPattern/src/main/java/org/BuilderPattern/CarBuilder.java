package org.BuilderPattern;

public class CarBuilder {
	public Car composeFerrari(){
		Car ferrari=new Car();
		ferrari.addItemCar(new AlloyWheel());
		ferrari.addItemCar(new Carbon());
		ferrari.addItemCar(new RW4());
		
		return ferrari;
	}
	
	public Car composeCinquecento(){
		Car cinquecento=new Car();
		cinquecento.addItemCar(new StandardWheel());
		cinquecento.addItemCar(new StandardChassis());
		cinquecento.addItemCar(new StandardEngine());
		
		return cinquecento;
	}
}
