package org.ChainOfResponsabilityPattern;

public enum CommandType {
	ProjectManager("Gestisci Progetto"),
	TeamLeader("Gestisci Team"),
	Developer("Programma");
	
	private final String commandType;
	
	CommandType(String type){
		this.commandType=type;
	}
	public String getCommandType() {
		return commandType;
	}	
}