package org.ChainOfResponsabilityPattern;

public abstract class AbstractEmployee {
	private CommandType commandType;
	private AbstractEmployee nextEmployee;
	
	public AbstractEmployee(CommandType c){
		commandType = c;
	}
	public void setNextEmployee(AbstractEmployee next ){
		nextEmployee=next;
	}
	
	public void run(CommandType command){
		if(command.equals(commandType))
			execute(command);
		else{
			System.out.println(this.toString()+"Non svolgo questa manzione, trasmetto il comando a "+nextEmployee.toString());
			nextEmployee.run(command);
		}
	}
	
	abstract void execute(CommandType command);
}
