package org.ChainOfResponsabilityPattern;


public class Client 
{
    public static void main( String[] args )
    {
     
    	Manager manager = new Manager(CommandType.ProjectManager);
    	TeamLeader teamLeader=new TeamLeader(CommandType.TeamLeader);
    	Developer developer=new Developer(CommandType.Developer);
    	
    	manager.setNextEmployee(teamLeader);
    	teamLeader.setNextEmployee(developer);
    	
    	manager.run(CommandType.Developer);
    	manager.run(CommandType.ProjectManager);
    	manager.run(CommandType.TeamLeader);
    	
	}
}