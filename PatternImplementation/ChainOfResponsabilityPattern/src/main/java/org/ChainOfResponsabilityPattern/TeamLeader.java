package org.ChainOfResponsabilityPattern;

public class TeamLeader extends AbstractEmployee {

	public TeamLeader(CommandType c) {
		super(c);
	}

	@Override
	void execute(CommandType command) {
		System.out.println("Sono il Team Leader, gestisco il Team assegnato!");

	}
	
	@Override
	public String toString() {
		return "Team Leader ";
	}

}
