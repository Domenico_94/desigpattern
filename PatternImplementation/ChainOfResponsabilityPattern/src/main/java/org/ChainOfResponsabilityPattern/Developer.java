package org.ChainOfResponsabilityPattern;

public class Developer extends AbstractEmployee {

	public Developer(CommandType c) {
		super(c);
	}

	@Override
	void execute(CommandType command) {
		System.out.println("Sono il Programmatore, gestisco il codice per il Programma assegnato!");

	}
	
	@Override
	public String toString() {
		return "Developer ";
	}

}
