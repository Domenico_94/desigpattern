package org.ChainOfResponsabilityPattern;

public class Manager extends AbstractEmployee {

	public Manager(CommandType c) {
		super(c);
	}

	@Override
	void execute(CommandType command) {
		System.out.println("Sono il Manager, gestisco il Progetto assegnato!");
	}

	@Override
	public String toString() {
		return "Manager ";
	}
	
	

}
