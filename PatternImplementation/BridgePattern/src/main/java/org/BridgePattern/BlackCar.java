package org.BridgePattern;

public class BlackCar implements ColorAPI {

	public void colorCar(int id, String type) {
		System.out.print("The Color Car "+type+" is Black!\n");
	}

}
