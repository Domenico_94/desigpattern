package org.BridgePattern;

public class RedCar implements ColorAPI {
	
	public void colorCar(int id, String type) {
		System.out.print("The Color Car "+type+" is Red!\n");
	}

}
