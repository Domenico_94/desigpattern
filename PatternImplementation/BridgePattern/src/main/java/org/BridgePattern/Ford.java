package org.BridgePattern;

public class Ford extends Veicle {
	private String type;
	private static int id=0;
	
	public Ford(String type, ColorAPI color) {
		super(color);
		id+=1;
		this.type=type;
	}
	@Override
	public void color() {
		colorApi.colorCar(getId(),type);

	}
	
	private int getId() {
		return id;
	}
	

}
