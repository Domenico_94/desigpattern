package org.BridgePattern;

public abstract class Veicle {
	protected ColorAPI colorApi;
	protected Veicle(ColorAPI colorApi){
		this.colorApi=colorApi;
	}
	
	public abstract void color();
}
