package org.BridgePattern;


public class Client 
{
    public static void main( String[] args )
    {
     Veicle redCar=new Ford("Fiesta", new RedCar());
     Veicle blackCar=new Ford("Focus", new BlackCar());
     
     redCar.color();
     blackCar.color();
    }
}