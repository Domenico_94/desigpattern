package org.AdapterPattern;

public interface Television {
	public void on(String type);
}
