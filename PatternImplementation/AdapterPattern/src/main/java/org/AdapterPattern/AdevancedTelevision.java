package org.AdapterPattern;

public interface AdevancedTelevision {
	public void onPremium(String type);
	public void onSky(String type);
}
