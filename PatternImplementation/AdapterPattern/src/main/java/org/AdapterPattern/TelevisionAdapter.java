package org.AdapterPattern;

public class TelevisionAdapter implements Television {
	AdevancedTelevision adevancedTelevision;
	
	public TelevisionAdapter(String type){
		if(type.equalsIgnoreCase("PREMIUM")){
			adevancedTelevision=new PremiumTelevision();
		}else if(type.equalsIgnoreCase("SKY")){
			adevancedTelevision=new SkyTelevision();
		}
	}

	public void on(String type) {
		if(type.equalsIgnoreCase("PREMIUM")){
			adevancedTelevision.onPremium("Premium");
		}else if(type.equalsIgnoreCase("SKY")){
			adevancedTelevision.onSky("Sky");
		}
	}
}
