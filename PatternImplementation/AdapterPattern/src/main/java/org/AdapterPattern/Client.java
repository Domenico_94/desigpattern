package org.AdapterPattern;

public class Client 
{
    public static void main( String[] args )
    {
    	TelevisionStarter television=new TelevisionStarter();
    	
    	television.on("Premium");
    	television.on("Analogico");
    	television.on("sky");
    }
}
