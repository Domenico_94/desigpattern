package org.AdapterPattern;

public class TelevisionStarter implements Television {
	TelevisionAdapter televisionAdapter;

	public void on(String type) {
		if(type.equalsIgnoreCase("ANALOGICO")){
			System.out.println("Hello, you jast running an Analogic Television!");
		}
		
		else if(type.equalsIgnoreCase("PREMIUM") || type.equalsIgnoreCase("SKY")){
			televisionAdapter= new TelevisionAdapter(type);
			televisionAdapter.on(type);
		}
		else 
			System.out.println("Invalid Type of Television: "+type);

	}

}
