package org.MementoPattern;

public class Client 
{
    public static void main( String[] args )
    {
    	CPU cpu=new CPU();
    	Memory memory=new Memory();
    	
    	
    	cpu.setState("Ready");
    	memory.add(cpu.saveStateToProcess());
    	

    	cpu.getStateFromProcess(memory.get(0));
    	memory.add(cpu.saveStateToProcess());
    	

    	
    	memory.get(1).setRunOrStuck(true);
    	cpu.getStateFromProcess(memory.get(1));

    	
    	memory.showMemoryProcess();
    	
    	memory.get(1).setRunOrStuck(false);
    	cpu.getStateFromProcess(memory.get(1));
    	
    	System.out.println("\nCurrent State: "+ cpu.getState());
    }
}
