package org.MementoPattern;

public class CPU {
	private String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public void getStateFromProcess(Process process){
		state=process.getState();
	}
	
	public Process saveStateToProcess(){
		Process process=new Process();
		process.setState(state);
		
		return process;
	}
}
