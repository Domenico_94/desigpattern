package org.MementoPattern;

import java.util.ArrayList;
import java.util.List;

public class Memory {
	private List<Process> memoryProcess=new ArrayList<Process>();
	
	public void add(Process process){
		memoryProcess.add(process);
	}
	
	public Process get(int index){
	      return memoryProcess.get(index);
	   }
	
	public void showMemoryProcess(){
		int i=1;
		for(Process process : memoryProcess){
			System.out.println("Saved State "+i+": "+process.getState());
			i++;
		}
	}

}
