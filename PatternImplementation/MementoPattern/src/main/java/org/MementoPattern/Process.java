package org.MementoPattern;

public class Process {
	private String state;
	private Boolean run;
	
	public Process() {
		setState("Ready");
		this.run=false;
	}

	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public Boolean getRun() {
		return run;
	}
	
	public void setRunOrStuck(Boolean run) {
		if(this.run == false && run == true){
			this.run = run;
			setState("Running");
		}else if(this.run == true && run == false){
			this.run = run;
			setState("Stuck");

		}
	}
}
