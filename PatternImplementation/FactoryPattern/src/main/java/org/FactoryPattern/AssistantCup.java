package org.FactoryPattern;

public class AssistantCup extends Assistant {
	public static IceCream getIceCreamCup(){
		return new IceCreamCup();
	}
}
