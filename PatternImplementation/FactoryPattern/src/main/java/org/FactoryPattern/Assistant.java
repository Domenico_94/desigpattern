package org.FactoryPattern;

public class Assistant {
	public IceCream getIceCream(String iceCreamType){
		if(iceCreamType.equalsIgnoreCase("CONO")){
			return AssistantCone.getIceCreamCone();
		}else if(iceCreamType.equalsIgnoreCase("COPPA")){
			return AssistantCup.getIceCreamCup();
		}
		
		return null;
	}
}
