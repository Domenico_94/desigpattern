package org.FactoryPattern;

public class Client 
{
    public static void main( String[] args )
    {
        Assistant assistant=new Assistant();
        IceCream iceCream=assistant.getIceCream("cono");
        
        iceCream.eat();
        
        
        iceCream=assistant.getIceCream("coppa");
        
        iceCream.eat();
    }
}
