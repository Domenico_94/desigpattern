package org.FactoryPattern;

public class AssistantCone extends Assistant {
	public static IceCream getIceCreamCone(){
		return new IceCreamCone();
	}
}
