package org.FlyWeightPattern;

public class Client 
{
	private static final String names[]={"Antonio" , "Raffaele" , "Mariagrazia" , "Domenico", "Giuseppe" , "Pietro"};
    private static final String defaultPass[]={"user" , "user1" , "user31" , "user51"}; 
	public static void main( String[] args )
    {
		for(int i=0;i<20;i++){
			StandardUser user=(StandardUser)UserFactory.getUser(randomName(), randomPass());
			
			user.login();
		}
     
    }
    
    private static String randomName(){
    	return names[(int)(Math.random()*names.length)];
    }
    
    private static String randomPass(){
    	return defaultPass[(int)(Math.random()*defaultPass.length)];
    }
}
