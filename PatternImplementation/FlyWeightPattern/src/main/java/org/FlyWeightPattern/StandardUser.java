package org.FlyWeightPattern;

public class StandardUser implements User {
	private String name;
	private String pass;

	public StandardUser(String name, String pass) {
		this.name = name;
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void login() {
		System.out.println("Login Successful!\n Hello "+name);

	}

}
