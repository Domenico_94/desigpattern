package org.FlyWeightPattern;

import java.util.HashMap;

public class UserFactory {
	private static final HashMap<String, User> userMap=new HashMap<String, User>();
	
	public static User getUser(String name, String pass){
		StandardUser user=(StandardUser)userMap.get(name);
		
		if(user==null){
			user=new StandardUser(name,pass);
			userMap.put(name, user);
			System.out.println("Created the user not mapped in the cache!");
		}
		return user;
	}
}
