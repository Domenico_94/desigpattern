package org.MediatorPattern;

import java.util.Date;

public class PersonalComputer {
	public static void sendEmail(User sender,User recipient,String email){
		System.out.println("Date: " + new Date() + "\nSender: " + sender.getName() + " Recipient: " + recipient.getName() + "\n" + email+"\n");
	}
}
