package org.MediatorPattern;


public class Client 
{
    public static void main( String[] args )
    {
     User sender=new User("Domenico","123");
     User recipient=new User("Pietro","345");
     
     sender.sendMyEmail(recipient,recipient.getName()+" your password has been changed: " + recipient.resetPassword());
     recipient.sendMyEmail(sender, "Thanks for the warning");
     
    }
}
