package org.MediatorPattern;

//This is the Mediator Class to use a PersonalComputer Class (the real class that send the Email)
public class User {
	private String name;
	private String password;
	private static final String defaultPass[]={"userPass" , "userPass1" , "userPass31" , "userPass51"}; 
	
	
	public User(String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	public String resetPassword() {
		this.password = defaultPass[(int)(Math.random()*defaultPass.length)];
		return this.password;
	}
	
	public void sendMyEmail(User recipient, String email){
		PersonalComputer.sendEmail(this,recipient,email);
	}
	
	

}
