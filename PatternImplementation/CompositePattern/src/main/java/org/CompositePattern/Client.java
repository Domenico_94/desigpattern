package org.CompositePattern;

public class Client 
{
    public static void main( String[] args )
    {
     Soldier supSoldier=new Soldier("Antonio Rossi","Generale");
     
     Soldier subSoldier=new Soldier("Michele Esposito","Tenente");
     
     Soldier sotSoldier=new Soldier("Raffaelle Armini", "Sotto Tenente");
     Soldier sotSoldier1=new Soldier("Antonio Topa", "Sotto Tenente");
     
     Soldier simSoldier=new Soldier("Giuseppe Tramontana", "Soldato Semplice");
     Soldier simSoldier1=new Soldier("Alexandro Luongo", "Soldato Semplice");
     
     supSoldier.add(subSoldier);
     
     subSoldier.add(sotSoldier);
     subSoldier.add(sotSoldier1);
     
     sotSoldier.add(simSoldier);
     sotSoldier.add(simSoldier1);
     
     sotSoldier1.add(simSoldier);
     sotSoldier1.add(simSoldier1);
     
     System.out.println(supSoldier);
     
     for(Soldier soldier:supSoldier.getSubordinates()){
    	 System.out.println("\t"+soldier);
     
	     //System.out.println("\n");
	     
	     for(Soldier soldierSub:soldier.getSubordinates()){
	    	 System.out.print("\n");
	    	 System.out.println("\t\t"+soldierSub);
	    	 
		     System.out.print("\n");
		     
		     for(Soldier soldierSot:soldierSub.getSubordinates()){
		    	 System.out.println("\t\t\t"+soldierSot);
		     }  
	     }  
     }
    }
}
