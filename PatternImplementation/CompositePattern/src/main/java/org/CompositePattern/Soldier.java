package org.CompositePattern;

import java.util.ArrayList;
import java.util.List;

public class Soldier {
	private String name;
	private String role;
	private List<Soldier> subordinates;
	
	public Soldier(String name, String role) {
		this.name=name;
		this.role=role;
		subordinates=new ArrayList<Soldier>();
	}
	
	public void add(Soldier soldier){
		subordinates.add(soldier);
	}
	
	public void remove(Soldier soldier){
		subordinates.remove(soldier);
	}
	
	public List<Soldier> getSubordinates(){
		return subordinates;
	}
	
	public String toString(){
		return ("[Soldier: "+name+", Role: "+role+"]");
	}
}
