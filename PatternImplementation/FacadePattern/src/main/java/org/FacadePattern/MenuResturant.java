package org.FacadePattern;

public interface MenuResturant {
	public void showMenu();
	public float bill();
}
