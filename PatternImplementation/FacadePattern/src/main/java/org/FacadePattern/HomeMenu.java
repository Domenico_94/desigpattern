package org.FacadePattern;

import java.util.ArrayList;
import java.util.List;

public class HomeMenu implements MenuResturant {
	
	private List<Flat> menu;
	

	public HomeMenu() {
		menu=new ArrayList<Flat>();
		addFlat(new Flat("Primo piatto della Casa",10f));
		addFlat(new Flat("Secondo piatto Casa",6f));
		addFlat(new Flat("Dolce della Casa",4f));
	}

	public List<Flat> getMenu() {
		return menu;
	}

	public void setMenu(List<Flat> menu) {
		this.menu = menu;
	}
	
	public void addFlat(Flat flat){
		menu.add(flat);
	}
	
	public void removeFlat(){
		menu.remove(menu.size());
	}

	public void showMenu() {
	for(Flat flat: menu){
		System.out.println(flat);
	}
	System.out.println("\t\t\t\t\t Total: "+bill()+"€");
	}

	public float bill() {
		float total=0f;
		for(Flat flat : menu){
		total=total+flat.getPrice();	
		}
		return total;
	}

}
