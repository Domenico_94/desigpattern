package org.FacadePattern;

public class Flat {
	private String type;
	private float price;
	
	public Flat(String type, float price) {
		super();
		this.type = type;
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public String toString(){
		return type+"\t\t\t"+price+"€";
	}
	
	

}
