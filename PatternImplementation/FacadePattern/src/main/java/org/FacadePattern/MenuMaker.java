package org.FacadePattern;

public class MenuMaker {
	private MenuResturant homeMenu;
	private MenuResturant seaMenu;
	private MenuResturant meatMenu;
	
	public MenuMaker() {
		homeMenu=new HomeMenu();
		seaMenu=new SeaMenu();
		meatMenu=new MeatMenu();
	}
	
	public void showHomeMenu(){
		homeMenu.showMenu();
	}
	
	public void showSeaMenu(){
		seaMenu.showMenu();
	}
	
	public void showMeatMenu(){
		meatMenu.showMenu();
	}
	
	public float getMenuHomePrice(){
		return homeMenu.bill();
	}
	public float getMenuSeaPrice(){
		return seaMenu.bill();
	}
	public float getMenuMeatPrice(){
		return meatMenu.bill();
	}
}
