package org.FacadePattern;

public class Client 
{
    public static void main( String[] args )
    {
     MenuMaker roomMaids=new MenuMaker();
     
     roomMaids.showHomeMenu();
     
     roomMaids.showMeatMenu();
     
     roomMaids.showSeaMenu();
    }
}
