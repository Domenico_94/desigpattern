package org.FacadePattern;

import static org.junit.Assert.*;
import org.junit.*;


/**
 * Unit test for FacadePattern.
 */
public class FacadePatternTest{
	  static float priceMenuHome = 0f;
	  static float priceMenuSea = 0f;
	  static float priceMenuMeat = 0f;
	 
	 
	@BeforeClass
	public static void initEnv(){
		priceMenuHome = 20f;
		priceMenuMeat = 29f;
		priceMenuSea = 39f;
	}
	
	@AfterClass
	public static void resetEnv(){
		priceMenuHome = 0f;
		priceMenuMeat = 0f;
		priceMenuSea = 0f;
	}
	

	@Test
    public void homeMenuSholdReturnPrice(){
 
        MenuMaker menuMaker=new MenuMaker();
        float total=menuMaker.getMenuHomePrice();
        assertEquals(priceMenuHome, total,0);
    }
	@Test
    public void seaMenuSholdReturnPrice(){
 
        MenuMaker menuMaker=new MenuMaker();
        float total=menuMaker.getMenuSeaPrice();
        assertEquals(priceMenuSea, total,0);
    }

	@Test
    public void meatMenuSholdReturnPrice(){
 
        MenuMaker menuMaker=new MenuMaker();
        float total=menuMaker.getMenuMeatPrice();
        assertEquals(priceMenuMeat, total,0);
    }
	
	@Test
    public void homeAndSeaMenuNotSholdReturnSamePrice(){
 
        MenuMaker menuMaker=new MenuMaker();
        float total=menuMaker.getMenuSeaPrice();
        assertNotEquals(priceMenuHome, total);
    }

}
