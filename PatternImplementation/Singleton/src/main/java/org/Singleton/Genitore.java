package org.Singleton;

public class Genitore implements Persona {
	private String name;
	public Genitore(String name) {
		this.name = name;
	}
	public void guida() {
		CarSingleton car = CarSingleton.getIstance();
		car.setMessage("Salve figli sono "+name+"! Ho preso la macchina. Ritorno per le 3 p.m.");
		car.guida();
	}

}
