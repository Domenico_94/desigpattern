package org.Singleton;

public class CarSingleton {
	
	private static CarSingleton car = new CarSingleton();
	private static int Id;
	private String message;
	private CarSingleton() {
		this.message = "";
	}
	
	synchronized public static CarSingleton getIstance() {
		Id=1;
		return car;
	}

	private String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void guida(){
		System.out.println(getMessage()+"\nId:"+Id);
	}
}
