package org.Singleton;


public class Client 
{
    public static void main( String[] args )
    {
    	Persona papa=new Genitore("Papà Antonio");
    	Persona figlio=new Figlio("Michele");
    	
    	for(int i=0;i<4;i++){
    		papa.guida();
    		
    		figlio.guida();
    	}
    	
    	
    }
}
