package org.Singleton;

public class Figlio implements Persona {
	
	private String name;
	public Figlio(String name) {
		this.name = name;
	}
	public void guida() {
		CarSingleton car = CarSingleton.getIstance();
		car.setMessage("Salve genitore sono "+name+"! Ho preso la macchina. Ritorno per le 1 a.m.");
		car.guida();
	}

}
