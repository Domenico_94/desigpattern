package org.ProxyPattern;

public class ServerProxy implements Connection {
	
	private Server server;
	private String connectionIP;
	

	public ServerProxy(String connectionIP) {
		this.connectionIP = connectionIP;
	}


	public String connect() {
		if(server == null){
			server=new Server(connectionIP);
		}
		return server.connect();
	}
}