package org.ProxyPattern;

public class Client 
{
    public static void main( String[] args )
    {
     Connection connection=new ServerProxy("127.0.0.1");
     
     //connection catch and connection 
     String IP=connection.connect();
     System.out.println("IP : "+IP+"\n");
     
     
     //connection without catch connection
     IP=connection.connect();
     System.out.println("IP : "+IP);
    }
}
