package org.ProxyPattern;

public interface Connection {
	public String connect();
}
