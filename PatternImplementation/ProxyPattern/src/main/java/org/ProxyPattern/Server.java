package org.ProxyPattern;

public class Server implements Connection {
	private String connectionIP;
	
	public Server(String connectionIP) {
		this.connectionIP = connectionIP;
		catchConnectionIP();
	}

	public String getConnectionIP() {
		return connectionIP;
	}

	public void setConnectionIP(String connectionIP) {
		this.connectionIP = connectionIP;
	}

	public String connect() {
		System.out.println("I'm Connect to:"+connectionIP);
		return connectionIP;
	}
	
	private void catchConnectionIP(){
		System.out.println("Catch Connection IP: "+connectionIP);
	}
}