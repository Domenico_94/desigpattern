package org.ProxyPattern;

import static org.junit.Assert.*;
import org.junit.*;

/**
 * Unit test for the connection of ServerProxy.
 */
public class ServerProxyTest{
	private String ip="125.0.0.2";
	@Test
    public void ServerProxyReturnCorrectIP(){
       Connection connection=new ServerProxy(ip);
       assertEquals(ip, connection.connect());
    }
}
